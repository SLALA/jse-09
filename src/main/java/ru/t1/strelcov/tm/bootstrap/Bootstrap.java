package ru.t1.strelcov.tm.bootstrap;

import ru.t1.strelcov.tm.api.ICommandController;
import ru.t1.strelcov.tm.api.ICommandRepository;
import ru.t1.strelcov.tm.api.ICommandService;
import ru.t1.strelcov.tm.constant.ArgumentConst;
import ru.t1.strelcov.tm.constant.TerminalConst;
import ru.t1.strelcov.tm.controller.CommandController;
import ru.t1.strelcov.tm.repository.CommandRepository;
import ru.t1.strelcov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(String... args) {
        displayWelcome();
        if (parseArgs(args))
            commandController.exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            parseCommand(scanner.nextLine());
            System.out.println();
        }
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.displaySystemInfo();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_INFO:
                commandController.displaySystemInfo();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

}
